# LaTeX base document

Dies ist das Master Dokument. Es enthält die Grundstruktur für Dokumente, die in Latex geschrieben werden. Dieses Dokument muss geforked werden und darf nur allgemeine Änderungen beinhalten.

## Latex Tutorials
- http://latex.hpfsc.de/content/latex_tutorial/
- http://latex.tugraz.at/latex/tutorial
 - kurz Anleitung: http://latex.tugraz.at/_media/docs/l2kurz.pdf 
(LateX Quelldatei: ftp://dante.ctan.org/tex-archive/info/lshort/german/)

## Wiki
See the [wiki page](https://bitbucket.org/jgrp/latex-base/wiki) for tutorials and details.